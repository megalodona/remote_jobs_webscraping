# REMOTE JOB SEARCH

## Objective
The objective of this project is to find remote jobs with a Junior profile in the Data Science sector. To achieve this goal, I will use LinkedIn search Jobs.

## Inspiration
There are several options in the LinkedIn Jobs filters, including remote, hybrid or on-site, and experience level. When choosing the remote and low experience options, senior or manager job ads appeared. I had to go from ad to ad and see what years of experience they were asking for. This process is tiresome and repetitive. That's how I came up with the idea of making a robot that would look at job offers every day and send by email those that met the following requirements:
- Data Scientist
- Remote
- Between 0 and 2 years of experience

## Steps
First, you enter the LinkedIn account, to do this, you have to write the email and password in the 'credentials.py' file. Once inside, access LinkedIn Jobs, choose the Remote filter and click Show results. The bot goes ad by ad looking for the number of years of professional experience in the description. If the job asks for between 0 and 2 years of professional experience, it will be saved in the dataset 'job_adds_details.csv'. This process is carried out daily, so it is normal that the vast majority of the jobs found are repeated. To avoid this, it just sends only the new ones found.
To monitor searches, we use the job_number file, which indicates the date of the last web-scraping and the total number of existing ads from the first web-scraping to the last one. When the robot sends the email it only chooses the newly added rows.


## Dataset details
**Job_adds_details.csv**
<br> Columns: {“description”: first 20 words of the job description,
<br>“n_ad”: ad number,
<br>“page”: number of the page where the ad is located,
<br>“years_exp”: years of experience}

**Number_jobs.json**
<br> Columns: {"date": "2022-05-20": last web-scraping date,
<br> "rows_adds": number of ads found since the first web-scraping without being repeated}

## Set up
1. Install packages with pip: -r requirements.txt

```
pip install -r requirements.txt

```
2. Install chrome driver:

https://chromedriver.chromium.org/downloads

My Google Chrome version: 102

3. Run main.py to start!



