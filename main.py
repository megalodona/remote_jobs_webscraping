# LOOK FOR JUNIOR REMOTE JOBS

# libraries
from credentials import USERNAME, PASSWORD, PASSWORD_MAIL
from functions_mail import *
import re as re
import time
import os.path
import pandas as pd
import json
from datetime import datetime

# web scraping libraries
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

# variables:
# date
date = datetime.today().strftime('%Y-%m-%d')
# which job position and where do you want to look?
position = 'data scientist junior'
local = 'españa'
# how many pages do you want to look?
first_page = 2  # first page is 2
n_pages_look = 3  # choose a number between 3 and 10
adds_per_page = 26  # number of ads per page in LinkedIn is 26

# webdriver set up
options = Options()
options.add_argument("window-size=1400,600")
driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
driver.set_window_size(1920, 1080)

# enter LinkedIn website with username and password
driver.get("https://www.linkedin.com/uas/login")
time.sleep(3)
email = driver.find_element(By.ID, "username")
email.send_keys(USERNAME)
password = driver.find_element(By.ID, "password")
password.send_keys(PASSWORD)
time.sleep(3)
password.send_keys(Keys.RETURN)


# opening jobs
driver.get(f"https://www.linkedin.com/jobs/search/?geoId=105646813&keywords={position}&location={local}")
time.sleep(2)

# go to filters and click remote button
all_filters = driver.find_element(By.XPATH, "/html/body/div[7]/div[3]/div[3]/section/div/div/div/div/div/button")
all_filters.click()
time.sleep(4)
remote_button = driver.find_element(By.XPATH, "/html/body/div[3]/div/div/div[2]/ul/li[6]/fieldset/div/ul/li[2]/label")
remote_button.click()
time.sleep(4)
show_results = driver.find_element(By.XPATH, "/html/body/div[3]/div/div/div[3]/div/button[2]/span")
show_results.click()
time.sleep(4)

# create a dataframe to save all the jobs that the robot finds
job_adds_df = pd.DataFrame(columns=['description', 'n_add', 'page', 'years_exp', 'url'])

# let's start doing webscraping page by page
for pages in range(first_page, n_pages_look):
    for num_add in range(1, adds_per_page):
        job_add = driver.find_element(By.XPATH,
                                      f"/html/body/div[7]/div[3]/div[3]/div[2]/div/section[1]/div/div/ul/li[{num_add}]/div/div/div[1]/div[2]/div[1]/a")
        time.sleep(4)
        job_add.click()
        time.sleep(2)
        job_description = driver.find_element(By.XPATH,
                                       f"/html/body/div[7]/div[3]/div[3]/div[2]/div/section[2]/div/div/div[1]/div/div[2]").text
        # look in each description the years of work experience
        units = '|'.join(["year", "years", "años", "año"])
        number = '\d+[.,]?\d*'
        plus_minus = '\+\/\-'
        cases = fr'({number})(?:[\s\d\-\+\/]*)(?:{units})'
        pattern = re.compile(cases)
        years_exp = pattern.findall(job_description)
        # if there is no information about work experience assume is 0
        if not years_exp:
            years_exp = ['0']
        years_exp = years_exp[0]
        years_exp = int(years_exp)
        # if the work experience needed is less than 2 add to the dataframe job_adds_df
        if years_exp < 2:
            print('Job found!')
            url = driver.current_url
            job_description = job_description.split()[0:20]
            job_description = ' '.join(job_description)
            newDfObj = pd.DataFrame({
                'description': job_description,
                'n_add': [num_add],
                'page': [pages],
                'years_exp': [years_exp],
                'url': [url]})
            job_adds_df = pd.concat([job_adds_df, newDfObj], ignore_index=True)
        # click next page
        if num_add == (adds_per_page - 1):
            driver.find_element(By.XPATH,
                                f"/html/body/div[7]/div[3]/div[3]/div[2]/div/section[1]/div/div/section/div/ul/li[{pages}]/button").click()

# monitoring job ads
if os.path.isfile('job_adds_details.csv'):
    print("File job_adds exist")
    past_job_adds_df = pd.read_csv('job_adds_details.csv', index_col=0)
    job_adds_df = pd.concat([past_job_adds_df, job_adds_df], axis=0)
    # json files and check if there are new jobs (rows)
    # if the jobs are duplicated they are removed from the dataframe
    job_adds_df = job_adds_df.drop_duplicates(subset=['description'])
    job_adds_df = job_adds_df.reset_index(drop=True)
    # save updated dataframe
    job_adds_df.to_csv('job_adds_details.csv')

# is created the first time the robot does web-scraping
else:
    print("File job_adds created")
    # remove duplicated jobs just in case
    job_adds_df = job_adds_df.drop_duplicates(subset=['description'])
    job_adds_df = job_adds_df.reset_index(drop=True)
    # save dataframe
    job_adds_df.to_csv('job_adds_details.csv')


# json where the number of jobs are updated
if os.path.isfile('number_jobs.json'):
    with open('number_jobs.json', 'r') as f:
        print("Json file exists")
        data = json.load(f)
        past_rows_add = data['rows_adds']
        total_number_adds = job_adds_df.shape[0]
        diff = total_number_adds - past_rows_add
# is created the first time the robot does web-scraping
else:
    with open('number_jobs.json', 'w') as f:
        print("New json file created")
        total_number_adds = job_adds_df.shape[0]
        json_new_adds = {
            "date": date,
            "rows_adds": total_number_adds}
        json.dump(json_new_adds, f)
        diff = total_number_adds

# if there are new jobs
if diff > 0:
    # update the json with the new number of job ads
    json_new_adds = {
        "date": date,
        "rows_adds": total_number_adds}

    with open('number_jobs.json', 'w') as f:
        json.dump(json_new_adds, f)

    last_n_url = pd.DataFrame(job_adds_df['url'].iloc[-diff:])
    # send EMAIL with jobs
    subject = 'New Jobs for you!'
    text = ''
    for index, row in last_n_url.iterrows():
        url_add = str(row['url']) + '\n'
        text = text + url_add
        text_mail = f"""Hello there! I have some jobs for you ;): {text}"""
    send_email(text=text_mail, subject=subject, USERNAME=USERNAME, PASSWORD_MAIL=PASSWORD_MAIL)

# if there are no new jobs
else:
    # send EMAIL with bad news :(
    subject = 'No Jobs for you!'
    text = 'Let see tomorrow :)'
    send_email(text=text, subject=subject, USERNAME=USERNAME, PASSWORD_MAIL=PASSWORD_MAIL)

